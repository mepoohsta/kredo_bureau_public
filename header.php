<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'kredo_bureau'); ?></a>



    <header class="header">
        <div id="sitePreloader" class="site-preloader">
            <svg width="200px"  height="200px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-bars" style="background: none;"><rect ng-attr-x="{{config.x1}}" y="30" ng-attr-width="{{config.width}}" height="40" fill="#3eb254" x="13" width="14"><animate attributeName="opacity" calcMode="spline" values="1;0.2;1" keyTimes="0;0.5;1" dur="1" keySplines="0.5 0 0.5 1;0.5 0 0.5 1" begin="-0.6s" repeatCount="indefinite"></animate></rect><rect ng-attr-x="{{config.x2}}" y="30" ng-attr-width="{{config.width}}" height="40" fill="#3eb254" x="33" width="14"><animate attributeName="opacity" calcMode="spline" values="1;0.2;1" keyTimes="0;0.5;1" dur="1" keySplines="0.5 0 0.5 1;0.5 0 0.5 1" begin="-0.4s" repeatCount="indefinite"></animate></rect><rect ng-attr-x="{{config.x3}}" y="30" ng-attr-width="{{config.width}}" height="40" fill="#3eb254" x="53" width="14"><animate attributeName="opacity" calcMode="spline" values="1;0.2;1" keyTimes="0;0.5;1" dur="1" keySplines="0.5 0 0.5 1;0.5 0 0.5 1" begin="-0.2s" repeatCount="indefinite"></animate></rect><rect ng-attr-x="{{config.x4}}" y="30" ng-attr-width="{{config.width}}" height="40" fill="#3eb254" x="73" width="14"><animate attributeName="opacity" calcMode="spline" values="1;0.2;1" keyTimes="0;0.5;1" dur="1" keySplines="0.5 0 0.5 1;0.5 0 0.5 1" begin="0s" repeatCount="indefinite"></animate></rect></svg>
        </div>
        <!-- /.site-preloader -->

        <div class="topline">
            <div class="container">
                <div class="topline_inner">
                    <div class="left">
                        <?php echo fw_get_db_settings_option('working_hours'); ?>
                    </div>
                    <!-- /.left -->
                    <div class="right">
                        <ul class="list">
                            <?php foreach (fw_get_db_settings_option('telephones') as $tel) { ?>
                                <li><a href="tel:<?php echo preg_replace('/[^0-9]/', '', $tel['tel']); ?>"><?php echo $tel['tel']; ?></a></li>
                            <?php } ?>
                            <?php if ($email = fw_get_db_settings_option('email')) { ?>
                                <li><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></li>
                            <?php } ?>
                        </ul>
                        <!-- /.list -->
                    </div>
                    <!-- /.right -->
                </div>
                <!-- /.topline_inner -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.topline -->


        <?php

        $banner_image = fw_get_db_settings_option('banner_image')['url'];

        if (is_post_type_archive() || (is_singular() && !is_front_page())) {
            $banner_image = fw_get_db_settings_option('banner_image_archive')['url'];
        }

        ?>

        <div class="banner" style="background-image: url('<?php echo $banner_image; ?>')">


        <div id="menus-container">
                <div class="desktop">
                    <div class="container">
                        <div class="inner">

                                <?php the_custom_logo(); ?>


                            <?php wp_nav_menu([
                            'theme_location'    => 'menu-1',
                            'menu_id'           => 'nav-menu-desktop',
                            'menu_class'        => 'nav-menu',
                            ]);
                            ?>

                            <a href="" class="btn btn-secondary" data-toggle="modal" data-target="#quickOrderModal"><?php echo fw_get_db_settings_option('header_button_text'); ?></a>
                            <!-- /.btn -->
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /.desktop -->
                <div class="mobile">
                    <div class="top">

                            <?php the_custom_logo(); ?>

                        <!-- /.logo-wrap -->
                        <button id="mobile-menu-burger" class="hamburger hamburger--collapse" type="button">
                        <span class="hamburger-box">
                          <span class="hamburger-inner"></span>
                        </span>
                        </button>
                    </div>
                    <!-- /.top -->
                    <div class="mid">
                        <?php wp_nav_menu([
                        'theme_location'    => 'menu-1',
                        'menu_id'           => 'nav-menu-mobile',
                        'menu_class'        => 'nav-menu',
                        ]);
                        ?>

                        <a href="" class="btn btn-secondary"  data-toggle="modal" data-target="#quickOrderModal"><?php echo fw_get_db_settings_option('header_button_text'); ?></a>

                        <div class="text">
                            <p><?php echo fw_get_db_settings_option('working_hours'); ?></p>
                            <ul>
                                <?php foreach (fw_get_db_settings_option('telephones') as $tel) { ?>
                                <li><a href="tel:<?php echo preg_replace('/[^0-9]/', '', $tel['tel']); ?>"><?php echo $tel['tel']; ?></a></li>
                                <?php } ?>

                                <?php if ($email = fw_get_db_settings_option('email')) { ?>
                                    <li><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <!-- /.text -->
                    </div>
                    <!-- /.bot -->
                </div>
                <!-- /.mobile -->
            </div>
            <!-- /.nav-menus-wrap -->

            <div class="container">
                <div class="row ">
                    <div class="col-xl-8 col-lg-7 col-md-6 col-sm-12">
                        <div class="banner_inner">
                            <div class="heading heading-h1 mb-md-0 mb-5">

                                <?php if (is_front_page()) { ?>
                                <h1 class="heading_title">
                                    <?php echo fw_get_db_settings_option('banner_text'); ?>
                                </h1>
                                <?php } elseif (is_post_type_archive() || is_home() || (is_singular() && !is_front_page())) { ?>
                                <h1 class="heading_title">
                                    <?php
                                        //the_archive_title( '<h1 class="heading_title">', '</h1>' );
                                    //Get post type
                                    $post_type_obj = get_post_type_object(get_post_type());
                                    //Get post type's label
                                    $title = apply_filters('post_type_archive_title', $post_type_obj->labels->name);
                                    echo $title;
                                    ?>
                                </h1>
                                <?php } ?>
                                <!-- /.heading_title -->
                            </div>
                            <!-- /.heading -->

                            <?php if (is_front_page()) { ?>

                            <div class="bot d-none d-md-block">
                                <div class="row">

                                    <?php foreach (fw_get_db_settings_option('content_repeater_banner') as $item) { ?>
                                    <div class="col-xl-4 col-lg-6">
                                        <div class="icon-elem">
                                            <div class="img-wrap">
                                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/banner-check.png" alt="<?php echo $item['text']; ?>">
                                            </div>
                                            <!-- /.img-wrap -->
                                            <div class="text">
                                                <?php echo $item['text']; ?>
                                            </div>
                                            <!-- /.text -->
                                        </div>
                                        <!-- /.icon-elem -->
                                    </div>
                                    <!-- /.col-lg-4 -->
                                    <?php } ?>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.bot -->
                            <?php } ?>
                        </div>
                        <!-- /.banner_inner -->
                    </div>
                    <!-- /.col-xl-8 col-lg-8 col-md-6 col-sm-12 -->

                    <?php if (is_front_page()) { ?>
                        <div class="col-xl-4 col-lg-5 col-md-6 col-sm-12">
                            <div id="form-calculator" class="form form-calculator" data-percent="<?php echo fw_get_db_settings_option('calculator_percent'); ?>">
                                <h3 class="title"><?php echo fw_get_db_settings_option('calculator_section_title'); ?></h3>
                                <div class="input-number-wrap">
                                    <div class="top">
                                        <label for="form-calculator_credit-amount"><?php echo fw_get_db_settings_option('calculator_sum_title'); ?>:</label>
                                        <!-- /.title -->
                                        <div class="input-number-wrapper">
                                            <input id="form-calculator_credit-amount" type="number" step="<?php echo fw_get_db_settings_option('calculator_sum_step'); ?>" value="<?php echo fw_get_db_settings_option('calculator_sum_value'); ?>" min="<?php echo fw_get_db_settings_option('calculator_sum_min'); ?>" max="<?php echo fw_get_db_settings_option('calculator_sum_max'); ?>">
                                        </div>
                                        <!-- /.input-number-wrapper -->
                                    </div>
                                    <!-- /.top -->
                                </div>
                                <!-- /.input-wrap -->

                                <div class="input-number-wrap">
                                    <div class="top">
                                        <label for="form-calculator_credit-expies"><?php echo fw_get_db_settings_option('calculator_term_title'); ?>:</label>
                                        <!-- /.title -->
                                        <div class="input-number-wrapper">
                                            <input id="form-calculator_credit-expies" type="number" value="<?php echo fw_get_db_settings_option('calculator_term_value'); ?>" min="<?php echo fw_get_db_settings_option('calculator_term_min'); ?>" max="<?php echo fw_get_db_settings_option('calculator_term_max'); ?>">
                                        </div>
                                        <!-- /.input-number-wrapper -->
                                    </div>
                                    <!-- /.top -->
                                </div>
                                <!-- /.input-wrap -->

                                <div class="credit-data-field">
                                    <span class="text"><?php echo fw_get_db_settings_option('calculator_sum_title'); ?>:</span>
                                    <!-- /.text -->
                                    <span class="num-wrap">
                                <span class="num" id="form-calculator_text__amount">5000</span>
                                        <!-- /.num -->
                                <span class="extension">руб.</span>
                                        <!-- /.extension -->
                            </span>
                                    <!-- /.num-wrap -->
                                </div>
                                <!-- /.credit-data-field -->

                                <div class="credit-data-field">
                                    <span class="text"><?php echo fw_get_db_settings_option('calculator_term_title'); ?>:</span>
                                    <!-- /.text -->
                                    <span class="num-wrap">
                                <span class="num" id="form-calculator_text__expies-days">5000</span>
                                        <!-- /.num -->
                                <span class="extension">мес.</span>
                                        <!-- /.extension -->
                            </span>
                                    <!-- /.num-wrap -->
                                </div>
                                <!-- /.credit-data-field -->

                                <div class="credit-data-field">
                                    <span class="text"><?php echo fw_get_db_settings_option('calculator_tax_title'); ?>:</span>
                                    <!-- /.text -->
                                    <span class="num-wrap">
                                <span class="num" id="form-calculator_text__mzda">5000</span>
                                        <!-- /.num -->
                                <span class="extension">руб.</span>
                                        <!-- /.extension -->
                            </span>
                                    <!-- /.num-wrap -->
                                </div>
                                <!-- /.credit-data-field -->

                                <div class="credit-data-field total">
                                    <span class="text"><?php echo fw_get_db_settings_option('calculator_back_title'); ?> <span id="form-calculator_text__expies-date"></span>:</span>
                                    <!-- /.text -->
                                    <span class="num-wrap">
                                <span class="num" id="form-calculator_text__total">5000</span>
                                        <!-- /.num -->
                                <span class="extension">руб.</span>
                                        <!-- /.extension -->
                            </span>
                                    <!-- /.num-wrap -->
                                </div>
                                <!-- /.credit-data-field -->

                                <button class="btn btn-primary" data-toggle="modal" data-target="#getCreditModal">
                                    <?php echo fw_get_db_settings_option('calculator_button_text'); ?>
                                </button>
                            </div>
                        </div>
                        <!-- /.col-xl-8 col-lg-8 col-md-6 col-sm-12 -->


                    <div class="col-12 d-md-none">
                        <div class="row">

                            <?php foreach (fw_get_db_settings_option('content_repeater_banner') as $item) { ?>
                                <div class="col-xl-4 col-lg-6">
                                    <div class="icon-elem">
                                        <div class="img-wrap">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/banner-check.png" alt="<?php echo $item['text']; ?>">
                                        </div>
                                        <!-- /.img-wrap -->
                                        <div class="text">
                                            <?php echo $item['text']; ?>
                                        </div>
                                        <!-- /.text -->
                                    </div>
                                    <!-- /.icon-elem -->
                                </div>
                                <!-- /.col-lg-4 -->
                            <?php } ?>
                            <!-- /.col-lg-4 -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.col-12 -->
                    <?php } ?>

                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.banner -->
    </header>
    <!-- /.header -->




