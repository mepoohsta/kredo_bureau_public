<?php
/**
 * Created by PhpStorm.
 * User: mepoo
 * Date: 05.10.2017
 * Time: 10:18.
 */
if (!defined('FW')) {
    die('Forbidden');
}
/*
 * @var $atts[] Array of attributes
 */
?>


<section class="box bg-with-stripes" id="blog">
    <div class="container">
        <div class="heading heading-h2 heading-left heading-mb-little heading-mb-big heading-with-buttons">

            <?php

            set_query_var('section_name', $atts['section_name']);
            set_query_var('section_title', $atts['section_title']);
            set_query_var('section_text', $atts['section_text']);

            get_template_part('template-parts/section', 'header');

            ?>

            <div class="buttons is-on-bot">
                <a href="<?php echo get_post_type_archive_link('news'); ?>"><?php echo $atts['link_text'] ?></a>
            </div>
            <!-- /.buttons -->
        </div>
        <!-- /.heading -->

        <div class="row justify-content-center">


            <?php $the_posts = get_posts(['post_type' => 'news', 'numberposts' => 4]);

            global $post;
            foreach ($the_posts as $post) :
                setup_postdata($post);

                ?>

                <div class="col-lg-3 col-md-4 col-sm-6">
                    <?php get_template_part('template-parts/content-archive', get_post_type()); ?>
                </div>
                <!-- /.col-lg-3 col-md-4 col-sm-6 -->
                <?php
                ?>


            <?php
            endforeach;
            wp_reset_postdata();

            ?>


        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
</section>
<!-- /.box -->
