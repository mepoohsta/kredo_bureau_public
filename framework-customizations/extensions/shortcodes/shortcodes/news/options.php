<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = [

    'link_text' => [
        'label' => __('Текст ссылки', 'fw'),
        'type'  => 'text',
    ],

];

$options = array_merge($options, unserialize(SECTION_TITLES));
