<?php
/**
 * Created by PhpStorm.
 * User: mepoo
 * Date: 05.10.2017
 * Time: 10:18.
 */
if (!defined('FW')) {
    die('Forbidden');
}
/*
 * @var $atts[] Array of attributes
 */
?>



<section>
    <div class="container">
        <div class="floating-from-wrapper">
            <div class="floating-from-wrap">
                <?php echo do_shortcode('[contact-form-7 id="5" title="Quick form" html_class="floating-from form"]'); ?>
            </div>
            <!-- /.floating-from-wrap -->

        </div>
        <!-- /.floating-from-wrap -->
    </div>
    <!-- /.container -->
</section>
<!-- /.floating-from-wrap -->


