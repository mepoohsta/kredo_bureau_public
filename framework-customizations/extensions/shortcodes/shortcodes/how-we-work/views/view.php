<?php
/**
 * Created by PhpStorm.
 * User: mepoo
 * Date: 05.10.2017
 * Time: 10:18.
 */
if (!defined('FW')) {
    die('Forbidden');
}

/*
 * @var $atts[] Array of attributes
 */
?>



<section class="box bg-gray-square" id="steps">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="img-wrap pl-md-4 pl-0 temp-class1">
                    <img src="<?php echo $atts['image']['url']; ?>" alt="Image">
                </div>
                <!-- /.img-wrap -->
            </div>
            <!-- /.col-md-6 -->
            <div class="col-lg-5 offset-lg-1 col-md-6">
                <div class="heading heading-h2 heading-left heading-mb-little heading-line">
                    <?php

                    set_query_var('section_name', $atts['section_name']);
                    set_query_var('section_title', $atts['section_title']);
                    set_query_var('section_text', $atts['section_text']);

                    get_template_part('template-parts/section', 'header');

                    ?>
                </div>
                <!-- /.heading -->
                <a href="" data-toggle="modal" data-target="#getCreditModal" class="btn btn-primary mt-4 mb-xl-5 mb-lg-4 mb-3"><?php echo $atts['button_text'] ?></a>
            </div>
            <!-- /.col-md-5 -->
        </div>
        <!-- /.row -->

        <div class="steps_wrap">
            <div class="row justify-content-center">

                <?php
                $count = 1;
                foreach ($atts['content_repeater'] as $key => $item) { ?>



                <div class="col-lg-4 col-md-6">
                    <div class="step-card text-invert">
                        <div class="img-wrap">
                            <img src="<?php echo $item['image']['url']; ?>" alt="<?php echo $item['title']; ?>">
                        </div>
                        <!-- /.img-wrap -->
                        <div class="descr">
                            <div class="heading heading-h5 heading-left mb-3">
                                <h5 class="heading_title">
                                    <?php echo $item['title']; ?>
                                </h5>
                                <!-- /.heading_title -->
                            </div>
                            <!-- /.heading -->
                            <p>
                                <?php echo $item['text']; ?>
                            </p>
                        </div>
                        <!-- /.descr -->
                    </div>
                    <!-- /.step -->
                </div>
                <!-- /.col-md-4 -->
            <?php $count++; } ?>



            </div>
            <!-- /.steps -->
        </div>
        <!-- /.steps_wrap -->


    </div>
    <!-- /.container -->
</section>
<!-- /.box -->


