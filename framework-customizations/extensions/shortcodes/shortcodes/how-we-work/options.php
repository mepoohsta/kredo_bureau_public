<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = [

    'button_text' => [
        'label' => __('Текст кнопки', 'fw'),
        'type'  => 'text',
    ],

    'content_repeater'      => [
        'type'    => 'group',
        'options' => [

            'content_repeater'              => [
                'attr'          => ['class' => 'button-advanced'],
                'type'          => 'addable-popup',
                'label'         => __('Добавить шаг', 'kredo_bureau'),
//                'desc'          => __( 'Добавление элементов', 'kredo_bureau' ),
                'template'      => '{{=title}}',
                'popup-options' => [
                    'sublist_group'  => [
                        'type'    => 'group',
                        'options' => [
                            'title'          => [
                                'label' => __('Заголовок', 'kredo_bureau'),
                                'desc'  => __('Введите текст', 'kredo_bureau'),
                                'type'  => 'text',
                            ],

                            'text'          => [
                                'label' => __('Основной текст', 'kredo_bureau'),
                                'desc'  => __('Введите текст', 'kredo_bureau'),
                                'type'  => 'text',
                            ],

                            'image' => [
                                'label'       => __('Изображение', 'fw'),
                                'images_only' => true,
                                'type'        => 'upload',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
];

$options = array_merge($options, unserialize(SECTION_TITLES));
$options = array_merge($options, unserialize(IMAGE));
