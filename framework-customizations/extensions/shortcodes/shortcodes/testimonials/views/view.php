<?php
/**
 * Created by PhpStorm.
 * User: mepoo
 * Date: 05.10.2017
 * Time: 10:18.
 */
if (!defined('FW')) {
    die('Forbidden');
}
/*
 * @var $atts[] Array of attributes
 */
?>


<section class="box before__bg-with-stripes" id="testimonials">
    <div class="container">
        <div class="heading heading-h2 heading-left heading-mb-little heading-mb-big heading-with-buttons">

            <?php

            set_query_var('section_name', $atts['section_name']);
            set_query_var('section_title', $atts['section_title']);
            set_query_var('section_text', $atts['section_text']);

            get_template_part('template-parts/section', 'header');

            ?>

            <div class="buttons is-on-bot">
                <div id="slick-carousel_testimonials__ids"></div>
                <!-- /#slick-carousel_testimonials__ids -->
            </div>
            <!-- /.buttons -->
        </div>
        <!-- /.heading -->
    </div>
    <!-- /.container -->


    <div class="slick-carousel slick-carousel_testimonials">


        <?php $the_posts = get_posts(['post_type' => 'testimonials']);

        global $post;
        foreach ($the_posts as $post) :
            setup_postdata($post);

            get_template_part('template-parts/content-archive', get_post_type());

        endforeach;
        wp_reset_postdata();

        ?>


    </div>
</section>
<!-- /.box -->
