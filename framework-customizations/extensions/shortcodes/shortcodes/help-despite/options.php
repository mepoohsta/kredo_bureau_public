<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = [

    'content_repeater'      => [
        'type'    => 'group',
        'options' => [

            'content_repeater'              => [
                'attr'          => ['class' => 'button-advanced'],
                'type'          => 'addable-popup',
                'label'         => __('Услуги', 'kredo_bureau'),
//                'desc'          => __( 'Добавление элементов', 'kredo_bureau' ),
                'template'      => '{{=text}}',
                'popup-options' => [
                    'sublist_group'  => [
                        'type'    => 'group',
                        'options' => [
                            'text'          => [
                                'label' => __('Текст', 'kredo_bureau'),
                                'desc'  => __('Введите текст', 'kredo_bureau'),
                                'type'  => 'textarea',
                            ],

                            'image'          => [
                                'label'       => __('Изображение', 'kredo_bureau'),
                                'desc'        => __('Введите изображение', 'kredo_bureau'),
                                'images_only' => true,
                                'type'        => 'upload',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
];

$options = array_merge($options, unserialize(SECTION_TITLES));
