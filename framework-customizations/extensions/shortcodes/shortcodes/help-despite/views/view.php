<?php
/**
 * Created by PhpStorm.
 * User: mepoo
 * Date: 05.10.2017
 * Time: 10:18.
 */
if (!defined('FW')) {
    die('Forbidden');
}

/*
 * @var $atts[] Array of attributes
 */
?>



<section class="box bg-dots-right" id="help">
    <div class="container">
        <div class="heading heading-h2 heading-left heading-line-extra heading-mb-big">



            <?php

            set_query_var('section_name', $atts['section_name']);
            set_query_var('section_title', $atts['section_title']);
            set_query_var('section_text', $atts['section_text']);

            get_template_part('template-parts/section', 'header');

            ?>
            <div class="heading_subtitle">
                <?php echo $section_name; ?>
            </div>
            <!-- /.heading_subtitle -->
            <h2 class="heading_title">
                <?php echo $section_title; ?> <b><?php echo $section_text; ?></b>
            </h2>
            <!-- /.heading_title -->
        </div>
        <!-- /.heading -->

        <div class="row">
            <?php  foreach ($atts['content_repeater'] as $key => $item) { ?>
            <div class="col-lg-4 col-md-6">
                <div class="help-card">
                    <div class="img-wrap">
                        <img src="<?php echo $item['image']['url']; ?>" alt="<?php echo $item['text']; ?>">
                    </div>
                    <!-- /.img-wrap -->
                    <div class="text">
                        <?php echo $item['text']; ?>
                    </div>
                    <!-- /.text -->
                </div>
                <!-- /.help-card -->
            </div>
            <!-- /.col-lg-4 -->
            <?php } ?>


        </div>
        <!-- /.row -->


        <?php get_template_part('template-parts/content-section', 'credit_type'); ?>

    </div>
    <!-- /.container -->
</section>
<!-- /.box -->

