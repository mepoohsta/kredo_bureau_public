<?php
/**
 * Created by PhpStorm.
 * User: mepoo
 * Date: 05.10.2017
 * Time: 10:18.
 */
if (!defined('FW')) {
    die('Forbidden');
}

/*
 * @var $atts[] Array of attributes
 */
?>


<section class="box bg-dots-and-green-curve text-invert" id="about">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6">
                <div class="about-image">
                    <img src="<?php echo $atts['image']['url'] ?>" alt="<?php echo $atts['section_name']; ?>">
                </div>
                <!-- /.about-image -->
                <div class="row mt-5">

                    <?php foreach ($atts['content_repeater'] as $key => $item) { ?>
                    <div class="col-lg-6 col-md-12 col-sm-6 col-12 text-center text-sm-left">
                        <div class="about-number">
                            <div class="num-wrap">
                                <div class="numz">
                                    <?php echo $item['big-text']; ?>
                                </div>
                                <!-- /.num -->
                                +
                            </div>
                            <!-- /.num-wrap -->
                            <div class="text">
                                <?php echo $item['text']; ?>
                            </div>
                            <!-- /.text -->
                        </div>
                        <!-- /.about-number -->
                    </div>
                    <!-- /.col-6 -->
                    <?php } ?>

                </div>
                <!-- /.row -->
            </div>
            <!-- /.col-xl-6 col-lg-6 -->
            <div class="col-xl-6 col-lg-6 col-md-6">
                <div class="heading heading-h2 heading-left heading-mb-big heading-line">
                    <?php

                    set_query_var('section_name', $atts['section_name']);
                    set_query_var('section_title', $atts['section_title']);
                    set_query_var('section_text', $atts['section_text']);

                    get_template_part('template-parts/section', 'header');

                    ?>
                </div>
                <!-- /.heading -->

                <?php echo $atts['info_text'] ?>
            </div>
            <!-- /.col-xl-6 col-lg-6 -->
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
</section>
<!-- /.box -->


