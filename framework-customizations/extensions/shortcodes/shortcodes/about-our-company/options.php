<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = [

    'form' => [
        'label' => __('Форма', 'fw'),
        'type'  => 'text',
    ],

    'info_text' => [
        'label'         => __('Основная информация', 'fw'),
        'type'          => 'wp-editor',
        'size'          => 'small', // small, large
        'editor_height' => 400,
        'wpautop'       => true,
        'editor_type'   => false, // tinymce, html

        /*
         * By default, you don't have any shortcodes into the editor.
         *
         * You have two possible values:
         *   - false:   You will not have a shortcodes button at all
         *   - true:    the default values you provide in wp-shortcodes
         *              extension filter will be used
         *
         *   - An array of shortcodes
         */
        'shortcodes' => false, // true, array('button', map')

        /*
         * Also available
         * https://github.com/WordPress/WordPress/blob/4.4.2/wp-includes/class-wp-editor.php#L80-L94
         */
    ],

    'content_repeater'      => [
        'type'    => 'group',
        'options' => [

            'content_repeater'              => [
                'attr'          => ['class' => 'button-advanced'],
                'type'          => 'addable-popup',
                'label'         => __('Главные плюсы', 'kredo_bureau'),
//                'desc'          => __( 'Добавление элементов', 'kredo_bureau' ),
                'template'      => '{{=text}}',
                'popup-options' => [
                    'sublist_group'  => [
                        'type'    => 'group',
                        'options' => [
                            'big-text'          => [
                                'label' => __('Большой текст', 'kredo_bureau'),
                                'desc'  => __('Введите текст', 'kredo_bureau'),
                                'type'  => 'text',
                            ],

                            'text'          => [
                                'label' => __('Основной текст', 'kredo_bureau'),
                                'desc'  => __('Введите текст', 'kredo_bureau'),
                                'type'  => 'text',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
];

$options = array_merge($options, unserialize(SECTION_TITLES));
$options = array_merge($options, unserialize(IMAGE));
