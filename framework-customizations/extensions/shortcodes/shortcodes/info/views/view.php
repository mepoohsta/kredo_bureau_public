<?php
/**
 * Created by PhpStorm.
 * User: mepoo
 * Date: 05.10.2017
 * Time: 10:18.
 */
if (!defined('FW')) {
    die('Forbidden');
}
/*
 * @var $atts[] Array of attributes
 */
?>

<section class="box box-double-bot" id="text">
    <div class="container">
        <div class="heading heading-h2 heading-left heading-line-extra heading-mb-big">

            <?php

            set_query_var('section_name', fw_get_db_settings_option('info_section_name'));
            set_query_var('section_title', fw_get_db_settings_option('info_section_title'));
            set_query_var('section_text', fw_get_db_settings_option('info_section_text'));

            get_template_part('template-parts/section', 'header');

            ?>


        </div>
        <!-- /.heading -->

        <div class="row justify-content-center">


            <?php $the_posts = get_posts(['post_type' => 'info']);

            global $post;
            foreach ($the_posts as $post) :
                setup_postdata($post);

                ?>

                <div class="col-lg-6">
                    <?php get_template_part('template-parts/content-archive', get_post_type()); ?>
                </div>
                <!-- /.col-lg-3 col-md-4 col-sm-6 -->
                <?php
                ?>


            <?php
            endforeach;
            wp_reset_postdata();

            ?>


        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
</section>
<!-- /.box -->
