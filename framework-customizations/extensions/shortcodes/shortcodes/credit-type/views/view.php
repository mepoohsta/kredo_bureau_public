<?php
/**
 * Created by PhpStorm.
 * User: mepoo
 * Date: 05.10.2017
 * Time: 10:18.
 */
if (!defined('FW')) {
    die('Forbidden');
}
/*
 * @var $atts[] Array of attributes
 */
?>



<section class="box box-double-bot" id="text">
    <div class="container">

        <?php

        if (
            !empty($atts['section_name']) ||
            !empty($atts['section_title']) ||
            !empty($atts['section_text'])

        ) { ?>

        <div class="heading heading-h2 heading-left heading-line-extra heading-mb-big">
            <?php

                set_query_var('section_name', $atts['section_name']);
                set_query_var('section_title', $atts['section_title']);
                set_query_var('section_text', $atts['section_text']);

                get_template_part('template-parts/section', 'header');

            ?>

        </div>
        <!-- /.heading -->
        <?php } ?>


        <?php get_template_part('template-parts/content-section', 'credit_type'); ?>
    </div>
    <!-- /.container -->
</section>
<!-- /.box -->

