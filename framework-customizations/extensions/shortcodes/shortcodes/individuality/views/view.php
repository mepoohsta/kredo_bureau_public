<?php
/**
 * Created by PhpStorm.
 * User: mepoo
 * Date: 05.10.2017
 * Time: 10:18.
 */
if (!defined('FW')) {
    die('Forbidden');
}
/*
 * @var $atts[] Array of attributes
 */
?>


<section class="box bg-with-zagogulina" id="advantages">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-8 col-md-6 d-flex align-items-center">
                <div class="heading heading-h1 heading-left heading-mb-little heading-line">


                    <?php

                    set_query_var('section_name', $atts['section_name']);
                    set_query_var('section_title', $atts['section_title']);
                    set_query_var('section_text', $atts['section_text']);

                    get_template_part('template-parts/section', 'header');

                    ?>


                </div>
                <!-- /.heading -->
            </div>
            <!-- /.col-md-6 -->

            <?php foreach ($atts['content_repeater'] as $key => $item) { ?>
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <div class="advantage-card">
                        <div class="img-wrap">
                            <img src="<?php echo $item['image']['url']; ?>" alt="<?php echo $item['title']; ?>">
                        </div>
                        <!-- /.img-wrap -->
                        <div class="heading heading-h4 heading-left heading-mb-little">
                            <h4 class="heading_title">
                                <?php echo $item['title']; ?>
                            </h4>
                            <!-- /.heading_title -->
                        </div>
                        <!-- /.heading -->
                        <p>
                            <?php echo $item['text']; ?>
                        </p>
                    </div>
                    <!-- /.advantage-card -->
                </div>
                <!-- /.col-lg-3 -->
            <?php } ?>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /.box -->





