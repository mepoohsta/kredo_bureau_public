<?php

if (!defined('FW')) {
    die('Forbidden');
}

$options = [

    'content_repeater'      => [
        'type'    => 'group',
        'options' => [

            'content_repeater'              => [
                'attr'          => ['class' => 'button-advanced'],
                'type'          => 'addable-popup',
                'label'         => __('Наши преимущества', 'kredo_bureau'),
//                'desc'          => __( 'Добавление элементов', 'kredo_bureau' ),
                'template'      => '{{=title}}',
                'popup-options' => [
                    'sublist_group'  => [
                        'type'    => 'group',
                        'options' => [
                            'title'          => [
                                'label' => __('Заголовок', 'kredo_bureau'),
                                'desc'  => __('Введите заголовок', 'kredo_bureau'),
                                'type'  => 'text',
                            ],

                            'text'          => [
                                'label' => __('Текст', 'kredo_bureau'),
                                'desc'  => __('Введите текст', 'kredo_bureau'),
                                'type'  => 'textarea',
                            ],

                            'image'          => [
                                'label'       => __('Изображение', 'kredo_bureau'),
                                'desc'        => __('Введите изображение', 'kredo_bureau'),
                                'images_only' => true,
                                'type'        => 'upload',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
];

$options = array_merge($options, unserialize(SECTION_TITLES));
