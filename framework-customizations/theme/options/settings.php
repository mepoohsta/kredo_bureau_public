<?php
/**
 * Created by PhpStorm.
 * User: Николай
 * Date: 01.06.2017
 * Time: 15:35.
 */
if (!defined('FW')) {
    die('Forbidden');
}

$options = [
    [

        'box_id' => [
            'type'  => 'box',
            'title' => __('Настройки', 'kredo_bureau'),

            'options' => [

                'email' => [
                    'label' => __('Email', 'kredo_bureau'),
                    'type'  => 'text',
                ],

                'telephones_group' => [
                    'type'    => 'group',
                    'options' => [

                        'telephones' => [
                            'type'          => 'addable-popup',
                            'label'         => __('Телефоны', 'kredo_bureau'),
                            'template'      => '{{=tel}}',
                            'popup-options' => [
                                'sublist_group' => [
                                    'type'    => 'group',
                                    'options' => [
                                        'tel' => [
                                            'label' => __('Телефон', 'kredo_bureau'),
                                            'type'  => 'text',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],

                'footer_bottomline_p1' => [
                    'type'  => 'textarea',
                    'label' => __('Футер нижняя правая строка', 'kredo_bureau'),
                ],

                'working_hours' => [
                    'type'  => 'text',
                    'label' => __('Часы работы', 'kredo_bureau'),
                ],

                'banner_image' => [
                    'label'       => __('Изображение в баннере', 'kredo_bureau'),
                    'desc'        => __('Укажите изображение', 'kredo_bureau'),
                    'images_only' => true,
                    'type'        => 'upload',
                ],

                'banner_image_archive' => [
                    'label'       => __('Изображение в баннере архива', 'kredo_bureau'),
                    'desc'        => __('Укажите изображение', 'kredo_bureau'),
                    'images_only' => true,
                    'type'        => 'upload',
                ],

                'banner_text' => [
                    'type'  => 'text',
                    'label' => __('Надпись в баннере на главной', 'kredo_bureau'),
                ],

                'header_button_text' => [
                    'type'  => 'text',
                    'label' => __('Надпись на кнопке в шапке', 'kredo_bureau'),
                ],

                'content_repeater_banner' => [
                    'type'    => 'group',
                    'options' => [

                        'content_repeater_banner' => [
                            'attr'          => ['class' => 'button-advanced'],
                            'type'          => 'addable-popup',
                            'label'         => __('Элементы в баннере', 'kredo_bureau'),
                            'template'      => '{{=text}}',
                            'popup-options' => [
                                'sublist_group' => [
                                    'type'    => 'group',
                                    'options' => [

                                        'text' => [
                                            'label' => __('Текст', 'kredo_bureau'),
                                            'desc'  => __('Введите текст', 'kredo_bureau'),
                                            'type'  => 'textarea',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],

                'info_section_name' => [
                    'label' => __('Название инфо секции ', 'kredo_bureau'),
                    'type'  => 'text',
                ],
                'info_section_title' => [
                    'label' => __('Заголовок  инфо секции ', 'fw'),
                    'type'  => 'text',
                ],
                'info_section_text' => [
                    'label' => __('Подзаголовок  инфо секции ', 'fw'),
                    'type'  => 'text',
                ],
            ],

        ],
    ],

    'box_calculator_id' => [
        'type'  => 'box',
        'title' => __('Настройки калькулятора', 'kredo_bureau'),

        'options' => [

            'calculator_section_title' => [
                'label' => __('Заголовок  калькулятора ', 'fw'),
                'type'  => 'text',
            ],

            'calculator_sum_title' => [
                'label' => __('Заголовок  сумма кредита ', 'fw'),
                'type'  => 'text',
            ],

            'calculator_sum_step' => [
                'label' => __('Шаг суммы ', 'fw'),
                'type'  => 'text',
            ],

            'calculator_sum_value' => [
                'label' => __('Значение по умолчанию ', 'fw'),
                'type'  => 'text',
            ],

            'calculator_sum_min' => [
                'label' => __('Минимальное значение', 'fw'),
                'type'  => 'text',
            ],

            'calculator_sum_max' => [
                'label' => __('Максимальное значение', 'fw'),
                'type'  => 'text',
            ],

            'calculator_term_title' => [
                'label' => __('Заголовок  срок кредита ', 'fw'),
                'type'  => 'text',
            ],

            'calculator_term_value' => [
                'label' => __('Значение по умолчанию ', 'fw'),
                'type'  => 'text',
            ],

            'calculator_term_min' => [
                'label' => __('Минимальное значение', 'fw'),
                'type'  => 'text',
            ],

            'calculator_term_max' => [
                'label' => __('Максимальное значение', 'fw'),
                'type'  => 'text',
            ],

            'calculator_tax_title' => [
                'label' => __('Заголовок плата за кредит ', 'fw'),
                'type'  => 'text',
            ],

            'calculator_back_title' => [
                'label' => __('Заголовок к возврату ', 'fw'),
                'type'  => 'text',
            ],

            'calculator_button_text' => [
                'label' => __('Надпись на кнопке ', 'fw'),
                'type'  => 'text',
            ],

            'calculator_percent' => [
                'label' => __('Процент кредита ', 'fw'),
                'type'  => 'text',
            ],

        ],
    ],

];
