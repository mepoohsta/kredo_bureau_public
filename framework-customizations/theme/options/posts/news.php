<?php
/**
 * Created by PhpStorm.
 * User: Николай
 * Date: 01.06.2017
 * Time: 15:35.
 */
if (!defined('FW')) {
    die('Forbidden');
}

$options = [
    [
        'box_id' => [
            'type'  => 'box',
            'title' => __('Настройки', 'kredo_bureau'),

            'options' => [
                'text'          => [
                    'label' => __('Источник', 'kredo_bureau'),
                    'desc'  => __('Введите источник', 'kredo_bureau'),
                    'type'  => 'text',
                ],
            ],
        ],

    ],
];
