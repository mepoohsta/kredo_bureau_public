<?php
/**
 * Created by PhpStorm.
 * User: Николай
 * Date: 01.06.2017
 * Time: 15:35.
 */
if (!defined('FW')) {
    die('Forbidden');
}

$options = [
    [

        'box_id' => [
            'type'  => 'box',
            'title' => __('Настройки', 'kredo_bureau'),

            'options' => [
                'content_repeater'      => [
                    'type'    => 'group',
                    'options' => [

                        'content_repeater'              => [
                            'attr'          => ['class' => 'button-advanced'],
                            'type'          => 'addable-popup',
                            'label'         => __('Элементы списка', 'kredo_bureau'),
                            'template'      => '{{=text}}',
                            'popup-options' => [
                                'sublist_group'  => [
                                    'type'    => 'group',
                                    'options' => [

                                        'text'          => [
                                            'label' => __('Основной текст', 'kredo_bureau'),
                                            'desc'  => __('Введите текст', 'kredo_bureau'),
                                            'type'  => 'text',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],

                'button_text'          => [
                    'label' => __('Текст на кнопке', 'kredo_bureau'),
                    'desc'  => __('Введите текст', 'kredo_bureau'),
                    'type'  => 'text',
                ],

            ],
        ],

    ],
];
