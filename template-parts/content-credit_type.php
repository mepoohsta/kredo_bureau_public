<?php $count = get_query_var('count');

?>
<div class="<?php echo ($count % 2 === 0) ? 'col-lg-5 col-md-8' : 'col-lg-7' ?>">
    <div class="product-card <?php echo ($count % 2 === 0) ? 'small' : '' ?>">
        <div class="left">
            <div class="heading heading-h3 heading-left heading-mb-little">
                <h3 class="heading_title">
                    <?php the_title(); ?>
                </h3>
                <!-- /.heading_title -->
            </div>
            <!-- /.heading -->


            <ul class="items">
                <?php foreach (fw_get_db_post_option(get_the_ID(), 'content_repeater') as $item) { ?>
                    <li class="item"><?php echo $item['text']; ?></li>
                <?php } ?>
            </ul>
            <!-- /.items -->
            <div class="btn btn-primary" data-toggle="modal" data-target="#getCreditModal">
                Оформить кредит
            </div>
            <!-- /.btn -->
        </div>
        <!-- /.left -->
        <div class="right">
            <div class="img-wrap">
                <?php the_post_thumbnail('full') ?>
            </div>
            <!-- /.img-wrap -->
        </div>
        <!-- /.right -->
    </div>
    <!-- /.product-card -->
</div>
