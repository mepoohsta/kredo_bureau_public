<?php

$section_name = get_query_var('section_name');
$section_title = get_query_var('section_title');
$section_text = get_query_var('section_text');

?>

<div class="heading_subtitle">
    <?php echo $section_name; ?>
</div>
<!-- /.heading_subtitle -->
<h2 class="heading_title">
    <?php echo $section_title; ?> <b><?php echo $section_text; ?></b>
</h2>
<!-- /.heading_title -->