<div class="row justify-content-center mt-5">
    <?php $the_posts = get_posts(['post_type' => 'credit_type']);

    $count = 0;
    global $post;
    foreach ($the_posts as $post) :
        setup_postdata($post); $count++;

        set_query_var('count', $count);
        get_template_part('template-parts/content', get_post_type());
        ?>



    <?php
    endforeach;
    wp_reset_postdata();

    ?>


</div>
<!-- /.row -->