<div class="carousel-elem">
    <div class="testimonial-card">
        <div class="top">
            <div class="img-wrap">
                <?php the_post_thumbnail('testimonial-thumbnail') ?>
            </div>
            <!-- /.img-wrap -->
            <div class="person">
                <div class="heading heading-h4 heading-left mb-2">
                    <h5 class="heading_title">
                        <?php the_title() ?>
                    </h5>
                    <!-- /.heading_title -->
                </div>
                <!-- /.heading -->
                <div class="date">
                    <?php echo get_the_date('j F Y') ?>
                </div>
                <!-- /.date -->
            </div>
            <!-- /.person -->
        </div>
        <!-- /.top -->
        
            <?php the_content() ?>

    </div>
    <!-- /.testimonial-card -->
</div>
<!-- /.carousel-elem -->