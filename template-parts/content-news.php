<?php
/**
 * Template part for displaying posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */
?>

<section class="box box-double-top bg-dots-right">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">

<article id="post-<?php the_ID(); ?>" <?php post_class('article-block'); ?>>


	<?php kredo_bureau_post_thumbnail(); ?>


    <header class="entry-header">

        <div class="entry-meta">
            <?php
            kredo_bureau_posted_on();
            kredo_bureau_posted_by();
            ?>
        </div><!-- .entry-meta -->


        <?php
        if (is_singular()) :
            the_title('<h1 class="entry-title">', '</h1>');
        else :
            the_title('<h2 class="entry-title"><a href="'.esc_url(get_permalink()).'" rel="bookmark">', '</a></h2>');
        endif;

        if ('post' === get_post_type()) :
            ?>

        <?php endif; ?>
    </header><!-- .entry-header -->

	<div class="post-single">
		<?php
        the_content(sprintf(
            wp_kses(
                /* translators: %s: Name of current post. Only visible to screen readers */
                __('Continue reading<span class="screen-reader-text"> "%s"</span>', 'kredo_bureau'),
                [
                    'span' => [
                        'class' => [],
                    ],
                ]
            ),
            get_the_title()
        ));

        wp_link_pages([
            'before' => '<div class="page-links">'.esc_html__('Pages:', 'kredo_bureau'),
            'after'  => '</div>',
        ]);
        ?>
	</div><!-- .entry-content -->


</article><!-- #post-<?php the_ID(); ?> -->
            </div>
            <!-- /.col-lg-6 -->
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
</section>
<!-- /.box -->