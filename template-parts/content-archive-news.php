<div class="post-card">
    <a href="<?php the_permalink(); ?>" class="img-wrap">
        <?php the_post_thumbnail('news-archive-thumbnail') ?>
    </a>
    <!-- /.img-wrap -->
    <div class="descr">
                            <span class="date">
                                <?php echo get_the_date('j F Y') ?>
                            </span>
        <!-- /.date -->
        <div class="heading heading-h4 heading-left">
            <a href="<?php the_permalink(); ?>" class="heading_title">
                <?php the_title() ?>
            </a>
            <!-- /.heading_title -->
        </div>
        <!-- /.heading -->
        <p>
            <?php the_excerpt() ?>
        </p>
    </div>
    <!-- /.descr -->
</div>
<!-- /.post-card -->