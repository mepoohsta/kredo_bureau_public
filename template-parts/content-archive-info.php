<div class="heading heading-h4 heading-left mb-2">
    <h5 class="heading_title">
        <?php the_title() ?>
    </h5>
    <!-- /.heading_title -->
</div>
<!-- /.heading -->

<?php the_content() ?>