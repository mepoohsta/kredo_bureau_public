<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */
get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php if (have_posts()) : ?>

            <section class="box box-double-top bg-dots-right">
                <div class="container">
                    <div class="row justify-content-center">

			<?php
            /* Start the Loop */
            while (have_posts()) :
                the_post();
?><?php
            ?><div class="col-lg-3 col-md-4 col-sm-6"><?php
                /*
                 * Include the Post-Type-specific template for the content.
                 * If you want to override this in a child theme, then include a file
                 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                 */
                get_template_part('template-parts/content-archive', get_post_type());
?>
                </div>
            <?php
            endwhile;

            the_posts_pagination();

        else :

            get_template_part('template-parts/content', 'none');

        endif;
        ?>


                        <!-- /.container -->
            </section>
            <!-- /.box -->

            <?php

            echo do_shortcode('[info]');
             ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
