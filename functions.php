<?php
/**
 * kredo_bureau functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */
if (!function_exists('kredo_bureau_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function kredo_bureau_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on kredo_bureau, use a find and replace
         * to change 'kredo_bureau' to the name of your theme in all the template files.
         */
        load_theme_textdomain('kredo_bureau', get_template_directory().'/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus([
            'menu-1' => esc_html__('Primary', 'kredo_bureau'),
        ]);

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', [
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ]);

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('kredo_bureau_custom_background_args', [
            'default-color' => 'ffffff',
            'default-image' => '',
        ]));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /*
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', [
            'height'      => 250,
            'width'       => 250,
            'flex-width'  => true,
            'flex-height' => true,
        ]);
    }
endif;
add_action('after_setup_theme', 'kredo_bureau_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function kredo_bureau_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('kredo_bureau_content_width', 640);
}

add_action('after_setup_theme', 'kredo_bureau_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function kredo_bureau_widgets_init()
{
    register_sidebar([
        'name'          => esc_html__('Sidebar', 'kredo_bureau'),
        'id'            => 'sidebar-1',
        'description'   => esc_html__('Add widgets here.', 'kredo_bureau'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ]);
}

add_action('widgets_init', 'kredo_bureau_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function kredo_bureau_scripts()
{
    wp_enqueue_style('kredo_bureau-style', get_stylesheet_uri());

    wp_enqueue_script('build', get_theme_file_uri('dist/build.js'), [], filemtime(get_theme_file_path('dist/build.js')));

    wp_enqueue_style('main', get_theme_file_uri('dist/main.css'), [], filemtime(get_theme_file_path('dist/main.css')));
}

add_action('wp_enqueue_scripts', 'kredo_bureau_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory().'/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory().'/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory().'/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory().'/inc/customizer.php';

/*
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory().'/inc/jetpack.php';
}

/*
 * Adding some const for fw
 */

define('SECTION_TITLES', serialize([
            'section_name' => [
                'label' => __('Название секции', 'the-core'),
                // 'desc'  => __( 'Введите Название секции', 'the-core' ),
                'type' => 'text',
            ],
            'section_title' => [
                'label' => __('Заголовок', 'fw'),
                'type'  => 'text',
            ],
            'section_text' => [
                'label' => __('Подзаголовок', 'fw'),
                'type'  => 'text',
            ],
        ]
    )
);

define('IMAGE', serialize([
            'image' => [
                'label'       => __('Изображение', 'the-core'),
                'desc'        => __('Введите изображение', 'the-core'),
                'images_only' => true,
                'type'        => 'upload',
            ],
        ]
    )
);

/*
 * Add credit_type type
 */

add_action('init', 'register_credit_type_post');
function register_credit_type_post()
{
    register_post_type('credit_type', [
        'labels' => [
            'name'          => 'Типы кредитов', // Основное название типа записи
            'singular_name' => 'Тип кредита', // отдельное название записи типа Book

        ],
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => ['title', 'thumbnail'],
    ]);
}

/*
 * Add news type
 */

add_action('init', 'register_news_post');
function register_news_post()
{
    register_post_type('news', [
        'labels' => [
            'name'          => 'Новости', // Основное название типа записи
            'singular_name' => 'Новость', // отдельное название записи типа Book
        ],
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => ['title', 'editor', 'author', 'thumbnail', 'excerpt'],
    ]);
}

/*
 * Add info testimonials type
 */
add_action('init', 'register_testimonials_post');
function register_testimonials_post()
{
    register_post_type('testimonials', [
        'labels' => [
            'name'          => 'Отзывы', // Основное название типа записи
            'singular_name' => 'Отзыв', // отдельное название записи типа Book
        ],
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => ['title', 'editor', 'thumbnail'],
    ]);
}

/*
 * Add info post type
 */
add_action('init', 'register_info_post');
function register_info_post()
{
    register_post_type('info', [
        'labels' => [
            'name'          => 'Информация', // Основное название типа записи
            'singular_name' => 'Информация', // отдельное название записи типа Book
        ],
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => ['title', 'editor'],
    ]);
}

/*
 * Adding image sizes
 */
//add_image_size('news-archive-thumbnail', 540, 360, true);
add_image_size('news-archive-thumbnail', 255, 170, true);
add_image_size('news-thumbnail', 730, 490, true);
add_image_size('testimonial-thumbnail', 50, 50, true);

/**
 * Filter except length to 10 words. tn custom excerpt length.
 *
 * @param $length
 *
 * @return int
 */
function tn_custom_excerpt_length($length)
{
    return 10;
}

add_filter('excerpt_length', 'tn_custom_excerpt_length', 999);

/**
 * Custom Scripting to Move JavaScript from the Head to the Footer.
 */
function remove_head_scripts()
{
    remove_action('wp_head', 'wp_print_scripts');
    remove_action('wp_head', 'wp_print_head_scripts', 9);
    remove_action('wp_head', 'wp_enqueue_scripts', 1);

    add_action('wp_footer', 'wp_print_scripts', 5);
    add_action('wp_footer', 'wp_enqueue_scripts', 5);
    add_action('wp_footer', 'wp_print_head_scripts', 5);
}

add_action('wp_enqueue_scripts', 'remove_head_scripts');

/*
 * Add scroll to menu items
 */
add_filter('nav_menu_link_attributes', 'wpse121123_contact_menu_atts', 10, 3);
function wpse121123_contact_menu_atts($atts, $item, $args)
{
    if (!empty($item->description) && is_front_page()) {
        $atts['data-scrollTo'] = '#'.$item->description;
    }

    return $atts;
}

/*
 * Remove unnecessary attribute
 */
add_filter('style_loader_tag', 'myplugin_remove_type_attr', 10, 2);
add_filter('script_loader_tag', 'myplugin_remove_type_attr', 10, 2);

function myplugin_remove_type_attr($tag, $handle)
{
    return preg_replace("/type=['\"]text\/(javascript|css)['\"]/", '', $tag);
}

add_action('template_redirect', function () {
    ob_start(function ($buffer) {
        $buffer = str_replace(['type="text/javascript"', "type='text/javascript'"], '', $buffer);

        return $buffer;
    });
});
