<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>

<section>
    <div class="container">
        <div class="floating-from-wrapper floating-from-styled">
            <div class="floating-from-wrap">
                <?php echo do_shortcode('[contact-form-7 id="5" title="Quick form" html_class="floating-from form"]'); ?>
            </div>
            <!-- /.floating-from-wrap -->

        </div>
        <!-- /.floating-from-wrap -->
    </div>
    <!-- /.container -->
</section>
<!-- /.floating-from-wrap -->

<!-- The Modal -->
<div class="modal" id="quickOrderModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"></button>
            <?php echo do_shortcode('[contact-form-7 id="63" title="Quick form modal" html_class="form"]'); ?>
        </div>
        <!-- /.modal-content -->
    </div>
</div>


<!-- The Modal -->
<div class="modal" id="getCreditModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"></button>
            <?php echo do_shortcode('[contact-form-7 id="64" title="Credit modal" html_class="form"]'); ?>
        </div>
        <!-- /.modal-content -->
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="top">

                <?php the_custom_logo(); ?>


            <div class="soc-list">
                <div class="img-wrap">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/image/soc-tel.png" alt="Telephone">
                </div>
                <!-- /.img-wrap -->
                <ul>
                    <?php foreach (fw_get_db_settings_option('telephones') as $tel) { ?>
                        <li><a href="tel:<?php echo preg_replace('/[^0-9]/', '', $tel['tel']); ?>"><?php echo $tel['tel']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>

            <!-- /.soc-list -->
            <?php wp_nav_menu([
                'theme_location'    => 'menu-1',
                'menu_id'           => 'nav-menu-footer',
                'menu_class'        => 'nav-menu',
            ]);
            ?>

        </div>
        <!-- /.top -->
        <div class="bot">
            <p>
                <?php echo '© '.get_bloginfo('name').' '.date('Y'); ?>
            </p>
            <p><?php echo fw_get_db_settings_option('footer_bottomline_p1'); ?></p>
        </div>
        <!-- /.bot -->
    </div>
    <!-- /.container -->
</footer>

<?php wp_footer(); ?>

</body>
</html>
